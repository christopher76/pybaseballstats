# pybaseballstats

A utility package to download baseball stats from
[Retrosheet](www.retrosheet.com) and [Sean Lahman](seanlahman.com) 
and import that data into a single Sqlite database to be used as a 
datasource for baseball stats visualizations.

This project is intended as a python learning exercise, not as 
the source of a published package.  This repository contains no distributions;
the package can be installed using the `pip intall git+url`method only.

This repository is not open to contributions.

## Usage

The following commands in a linux shell should generate a file named baseball.sqlite.

    mkdir pbs
    cd pbs
    python3 -m venv venv
    source venv/bin/activate
    pip install git+https://<TODO>
    python3 -m pybaseballstats

## Data Sources

### [Retrosheet](www.retrosheet.org)

The information used here was obtained free of 
charge from and is copyrighted by Retrosheet.  
Interested parties may contact Retrosheet at 
"[www.retrosheet.org](https://www.retrosheet.org/)".

### [Sean Lahman](http://seanlahman.com/)


This database is copyright 1996-2024 by Sean Lahman.
This work is licensed under a Creative Commons Attribution-ShareAlike 3.0
Unported License. For details see: http://creativecommons.org/licenses/by-sa/3.0/.
For licensing information or further information, contact Sean Lahman at: seanlahman@gmail.com

## Other Resources:

* [Similar Projects for Retrosheet](https://www.retrosheet.org/resources/resources1.html)
* [Similar Project for Lahman (MySql)](https://github.com/WebucatorTraining/lahman-baseball-mysql)
* [Alternate Data Source for Lahman (Sqlite)](https://github.com/jknecht/baseball-archive-sqlite)