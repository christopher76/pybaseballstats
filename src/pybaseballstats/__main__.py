import sys

from pybaseballstats import download_and_import

if __name__ == "__main__":
    clear = "-c" in sys.argv
    download_and_import(clear_downloads=clear)
