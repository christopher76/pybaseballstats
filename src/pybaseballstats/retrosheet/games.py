from datetime import datetime

from pybaseballstats.importer import text_or_none, int_or_none

META_URL = "https://www.retrosheet.org/gamelogs/glfields.txt"
DATA_URL = "https://www.retrosheet.org/gamelogs/gl1871_2023.zip"


TABLE_NAME = "rs_games"

TABLE = """CREATE TABLE IF NOT EXISTS rs_games
                (
                id                                 INTEGER NOT NULL PRIMARY KEY,
                game_date                          TEXT    NOT NULL, --0
                year_id                            INTEGER NOT NULL, --0
                month_id                           INTEGER NOT NULL, --0
                day_id                             INTEGER NOT NULL, --0
                game_number                        INTEGER NOT NULL, --1
                visiting_team_id                   INTEGER NOT NULL, --3, 4
                visiting_game_number               INTEGER NOT NULL, --5
                home_team_id                       INTEGER NOT NULL, --6, 7
                home_game_number                   INTEGER NOT NULL, --8
                visiting_score                     INTEGER NOT NULL, --9
                home_score                         INTEGER NOT NULL, --10
                game_length_outs                   INTEGER,          --11
                day_night                          TEXT,             --12
                completion_info                    TEXT,             --13
                forfeit_info                       TEXT,             --14
                protest_info                       TEXT,             --15
                park_id                            INTEGER NOT NULL, --16
                attendance                         INTEGER,          --17
                game_length_minutes                INTEGER,          --18
                visiting_line_score                TEXT,             --19
                home_line_score                    TEXT,             --20
                visiting_at_bats                   INTEGER,          --21
                visiting_hits                      INTEGER,          --22
                visiting_doubles                   INTEGER,          --23
                visiting_triples                   INTEGER,          --24
                visiting_homeruns                  INTEGER,          --25
                visiting_rbi                       INTEGER,          --26
                visiting_sacrifice_hits            INTEGER,          --27
                visiting_sacrifice_flies           INTEGER,          --28
                visiting_hit_by_pitch              INTEGER,          --29
                visiting_walks                     INTEGER,          --30
                visiting_intentional_walks         INTEGER,          --31
                visiting_strikeouts                INTEGER,          --32
                visiting_stolen_bases              INTEGER,          --33
                visiting_caught_stealing           INTEGER,          --34
                visiting_grounded_into_double_play INTEGER,          --35
                visiting_catcher_interference      INTEGER,          --36
                visiting_left_on_base              INTEGER,          --37
                visiting_pitchers_used             INTEGER,          --38
                visiting_ind_earned_runs           INTEGER,          --39
                visiting_teams_earned_runs         INTEGER,          --40
                visiting_wild_pitches              INTEGER,          --41
                visiting_balks                     INTEGER,          --42
                visiting_putouts                   INTEGER,          --43
                visiting_assists                   INTEGER,          --44
                visiting_errors                    INTEGER,          --45
                visiting_passed_balls              INTEGER,          --46
                visiting_double_plays              INTEGER,          --47
                visiting_triple_plays              INTEGER,          --48
                home_at_bats                       INTEGER,          --49
                home_hits                          INTEGER,          --50
                home_doubles                       INTEGER,          --51
                home_triples                       INTEGER,          --52
                home_homeruns                      INTEGER,          --53
                home_rbi                           INTEGER,          --54
                home_sacrifice_hits                INTEGER,          --55
                home_sacrifice_flies               INTEGER,          --56
                home_hit_by_pitch                  INTEGER,          --57
                home_walks                         INTEGER,          --58
                home_intentional_walks             INTEGER,          --59
                home_strikeouts                    INTEGER,          --60
                home_stolen_bases                  INTEGER,          --61
                home_caught_stealing               INTEGER,          --62
                home_grounded_into_double_play     INTEGER,          --63
                home_catcher_interference          INTEGER,          --64
                home_left_on_base                  INTEGER,          --65
                home_pitchers_used                 INTEGER,          --66
                home_ind_earned_runs               INTEGER,          --67
                home_teams_earned_runs             INTEGER,          --68
                home_wild_pitches                  INTEGER,          --69
                home_balks                         INTEGER,          --70
                home_putouts                       INTEGER,          --71
                home_assists                       INTEGER,          --72
                home_errors                        INTEGER,          --73
                home_passed_balls                  INTEGER,          --74
                home_double_plays                  INTEGER,          --75
                home_triple_plays                  INTEGER,          --76
                umpire_home_id                     INTEGER,          --77 
                umpire_first_id                    INTEGER,          --79
                umpire_second_id                   INTEGER,          --81 
                umpire_third_id                    INTEGER,          --83
                umpire_left_id                     INTEGER,          --85 
                umpire_right_id                    INTEGER,          --87 
                visiting_manager_id                INTEGER NOT NULL, --89
                home_manager_id                    INTEGER NOT NULL, --91
                pitcher_winning_id                 INTEGER,          --93
                pitcher_losing_id                  INTEGER,          --95
                pitcher_saving_id                  INTEGER,          --97
                batter_rbi_winner_id               INTEGER,          --99
                visiting_pitcher_starting_id       INTEGER,          --101
                home_pitcher_starting_id           INTEGER,          --103
                visiting_starter_1_id              INTEGER,          --105
                visiting_starter_1_pos             INTEGER,          --107
                visiting_starter_2_id              INTEGER,          --108
                visiting_starter_2_pos             INTEGER,          --110
                visiting_starter_3_id              INTEGER,          --111
                visiting_starter_3_pos             INTEGER,          --113
                visiting_starter_4_id              INTEGER,          --114
                visiting_starter_4_pos             INTEGER,          --116
                visiting_starter_5_id              INTEGER,          --117
                visiting_starter_5_pos             INTEGER,          --119
                visiting_starter_6_id              INTEGER,          --120
                visiting_starter_6_pos             INTEGER,          --121
                visiting_starter_7_id              INTEGER,          --123
                visiting_starter_7_pos             INTEGER,          --125
                visiting_starter_8_id              INTEGER,          --126
                visiting_starter_8_pos             INTEGER,          --127
                visiting_starter_9_id              INTEGER,          --129
                visiting_starter_9_pos             INTEGER,          --131
                home_starter_1_id                  INTEGER,          --132
                home_starter_1_pos                 INTEGER,          --134
                home_starter_2_id                  INTEGER,          --135
                home_starter_2_pos                 INTEGER,          --137
                home_starter_3_id                  INTEGER,          --138
                home_starter_3_pos                 INTEGER,          --140
                home_starter_4_id                  INTEGER,          --141
                home_starter_4_pos                 INTEGER,          --143
                home_starter_5_id                  INTEGER,          --144
                home_starter_5_pos                 INTEGER,          --146
                home_starter_6_id                  INTEGER,          --147
                home_starter_6_pos                 INTEGER,          --149
                home_starter_7_id                  INTEGER,          --150
                home_starter_7_pos                 INTEGER,          --152
                home_starter_8_id                  INTEGER,          --153
                home_starter_8_pos                 INTEGER,          --155
                home_starter_9_id                  INTEGER,          --156
                home_starter_9_pos                 INTEGER,          --158
                additional_info                    TEXT,             --159
                acquisition_info                   TEXT    NOT NULL  --160
            );"""


def row_function(lookup_team, lookup_player, lookup_park, row):
    date = datetime.strptime(row[0], "%Y%m%d")  # parsing the date string
    return (
        None,
        text_or_none(row[0]),
        date.year,
        date.month,
        date.day,
        int_or_none(row[1]),
        lookup_team(*row[3:5]),
        int_or_none(row[5]),
        lookup_team(*row[6:8]),
        *int_or_none(row[8:12]),
        *text_or_none(row[12:16]),
        lookup_park(row[16]),
        *int_or_none(row[17:19]),
        *text_or_none(row[19:21]),
        *int_or_none(row[21:77]),
        lookup_player(row[77]),
        lookup_player(row[79]),
        lookup_player(row[81]),
        lookup_player(row[83]),
        lookup_player(row[85]),
        lookup_player(row[87]),
        lookup_player(row[89]),
        lookup_player(row[91]),
        lookup_player(row[93]),
        lookup_player(row[95]),
        lookup_player(row[97]),
        lookup_player(row[99]),
        lookup_player(row[101]),
        lookup_player(row[103]),
        lookup_player(row[105]),
        int_or_none(row[107]),
        lookup_player(row[108]),
        int_or_none(row[110]),
        lookup_player(row[111]),
        int_or_none(row[113]),
        lookup_player(row[114]),
        int_or_none(row[116]),
        lookup_player(row[117]),
        int_or_none(row[119]),
        lookup_player(row[120]),
        int_or_none(row[122]),
        lookup_player(row[123]),
        int_or_none(row[125]),
        lookup_player(row[126]),
        int_or_none(row[128]),
        lookup_player(row[129]),
        int_or_none(row[131]),
        lookup_player(row[132]),
        int_or_none(row[134]),
        lookup_player(row[135]),
        int_or_none(row[137]),
        lookup_player(row[138]),
        int_or_none(row[140]),
        lookup_player(row[141]),
        int_or_none(row[143]),
        lookup_player(row[144]),
        int_or_none(row[146]),
        lookup_player(row[147]),
        int_or_none(row[149]),
        lookup_player(row[150]),
        int_or_none(row[152]),
        lookup_player(row[153]),
        int_or_none(row[155]),
        lookup_player(row[156]),
        int_or_none(row[158]),
        *text_or_none(row[159:]),
    )
