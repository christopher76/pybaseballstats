import re
from functools import partial
from pathlib import Path

from pybaseballstats.retrosheet import parks, players, games, teams
from pybaseballstats.importer import Archive, Importer

# Retrosheet
# https://www.retrosheet.org/gamelogs/index.html
# The information used here was obtained free of
# charge from and is copyrighted by Retrosheet.  Interested
# parties may contact Retrosheet at "www.retrosheet.org".


def load(context: Importer):

    context.confirm_download(teams.META_URL)
    context.execute_nonquery(teams.TABLE)
    teams_file = context.confirm_download(teams.DATA_URL, change_extension="csv")
    print("Processing ", teams_file)
    context.load_table_from_file(
        teams_file,
        teams.TABLE_NAME,
        teams.row_function,
        skip_first_row=False,
    )
    for index in teams.INDEXES:
        context.execute_nonquery(index)

    context.confirm_download(players.META_URL)
    context.execute_nonquery(players.TABLE)
    print("Processing ", players.FILE_NAME)
    context.load_table_from_archive(
        Archive(context.confirm_download(players.DATA_URL)),
        players.FILE_NAME,
        players.TABLE_NAME,
        players.row_function,
        skip_first_row=False,
    )
    for index in players.INDEXES:
        context.execute_nonquery(index)

    context.execute_nonquery(parks.TABLE)
    print("Processing ", parks.FILE_NAME)
    context.load_table_from_archive(
        Archive(context.confirm_download(parks.DATA_URL)),
        parks.FILE_NAME,
        parks.TABLE_NAME,
        parks.row_function,
        skip_first_row=False,
    )
    for index in parks.INDEXES:
        context.execute_nonquery(index)

    context.confirm_download(games.META_URL)
    context.execute_nonquery(games.TABLE)
    archive = Archive(context.confirm_download(games.DATA_URL))
    games_row_function = partial(
        games.row_function,
        partial(teams.lookup, context),
        partial(players.lookup, context),
        partial(parks.lookup, context),
    )
    game_files = [
        fn
        for fn in archive.get_names()
        if re.match(r"gl[0-9]{4}", Path(fn).stem)
        or Path(fn).stem in ["glwc.txt", "gldv.txt", "gllc.txt", "glws.txt"]
    ]
    print("Processing game files...")
    for file in game_files:
        context.load_table_from_archive(
            archive, file, games.TABLE_NAME, games_row_function, skip_first_row=False
        )
