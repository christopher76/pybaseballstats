from functools import cache

from pybaseballstats.importer import text_or_none, Importer

META_URL = "https://www.retrosheet.org/biofile.htm"
DATA_URL = "https://www.retrosheet.org/biofile.zip"

FILE_NAME = "biofile.csv"

TABLE_NAME = "rs_players"

TABLE = """CREATE TABLE IF NOT EXISTS rs_players
            (
                id                INTEGER NOT NULL PRIMARY KEY,
                player_id         TEXT    NOT NULL,
                last_name         TEXT    NOT NULL,
                first_name        TEXT,
                nickname          TEXT,
                birth_date        TEXT,
                birth_city        TEXT,
                birth_state       TEXT,
                birth_country     TEXT,
                player_debut      TEXT,
                player_last_game  TEXT,
                manager_debut     TEXT,
                manager_last_game TEXT,
                coach_debut       TEXT,
                coach_last_game   TEXT,
                umpire_debut      TEXT,
                umpire_last_game  TEXT,
                death_date        TEXT,
                death_city        TEXT,
                death_state       TEXT,
                death_country     TEXT,
                bats              TEXT,
                throws            TEXT,
                height            TEXT,
                weight            TEXT,
                cemetery_name     TEXT,
                cemetery_city     TEXT,
                cemetery_state    TEXT,
                cemetery_country  TEXT,
                cemetery_note     TEXT,
                birth_name        TEXT,
                name_change       TEXT,
                bat_change        TEXT,
                hof               TEXT,
                UNIQUE (player_Id)
            );"""

INDEXES = ["""CREATE INDEX idx_rs_players_1 ON rs_players (player_id);"""]


def row_function(row):
    return (
        None,
        *text_or_none(row),
    )


@cache
def lookup(context: Importer, player_id):
    if not player_id:
        return None

    if player_id == "(none)":
        return player_id

    row = context.execute_single(
        "SELECT id FROM rs_players WHERE player_id = ?;", (player_id,)
    )
    if row:
        return row[0]
    else:
        raise KeyError
