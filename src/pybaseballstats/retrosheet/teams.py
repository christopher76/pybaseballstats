from functools import cache

from pybaseballstats.importer import text_or_none, int_or_none, Importer

META_URL = "https://www.retrosheet.org/TeamIDs.htm"
DATA_URL = "https://www.retrosheet.org/TEAMABR.TXT"

TABLE_NAME = "rs_teams"

TABLE = """CREATE TABLE IF NOT EXISTS rs_teams
            (
                id         INTEGER NOT NULL PRIMARY KEY,
                abbr       TEXT    NOT NULL,
                league     TEXT    NOT NULL,
                city       TEXT    NOT NULL,
                name       TEXT    NOT NULL,
                first_year INTEGER NOT NULL,
                last_year  INTEGER NOT NULL,
                UNIQUE (abbr, league)
            );"""

INDEXES = ["""CREATE INDEX idx_rs_teams_1 ON rs_teams (abbr, league);"""]


def row_function(row):
    return (
        None,
        *text_or_none(row[0:4]),
        *int_or_none(row[4:]),
    )


@cache
def lookup(context: Importer, abbr, league):
    # individual fixes
    if (abbr, league) == ("BL5", "AA"):
        abbr = "BL2"

    row = context.execute_single(
        "SELECT id FROM rs_teams WHERE abbr = ? AND league = ?;", (abbr, league)
    )
    if row:
        return row[0]
    else:
        raise KeyError
