from functools import cache

from pybaseballstats.importer import text_or_none, Importer

DATA_URL = "https://www.retrosheet.org/ballparks.zip"

FILE_NAME = "ballparks.csv"

TABLE_NAME = "rs_parks"

TABLE = """
        CREATE TABLE IF NOT EXISTS rs_parks
        (
            id      INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
            park_id TEXT    NOT NULL,
            name    TEXT,
            alias   TEXT,
            city    TEXT,
            state   TEXT,
            start   TEXT,
            end     TEXT,
            league  TEXT,
            notes   TEXT,
            UNIQUE (park_id)
        );
        """

INDEXES = ["""CREATE INDEX idx_rs_parks_1 ON rs_parks (park_id);"""]


def row_function(row):
    return (
        None,
        *text_or_none(row),
    )


@cache
def lookup(context: Importer, park_id):
    row = context.execute_single(
        "SELECT id FROM rs_parks WHERE park_id = ?;", (park_id,)
    )
    if row:
        return row[0]
    else:
        raise KeyError
