from pybaseballstats.importer import int_or_none

FILE_NAME = "lahman_1871-2023_csv/FieldingOF.csv"

TABLE_NAME = "l_fielding_of"

TABLE = """CREATE TABLE IF NOT EXISTS l_fielding_of
            (
                player_id TEXT    NOT NULL,
                year      TEXT    NOT NULL,
                stint     INTEGER NOT NULL,
                Glf       INTEGER,
                Gcf       INTEGER,
                Grf       INTEGER,
                PRIMARY KEY (player_id, year, stint)
            );"""


def row_function(row, player, **kwargs):
    return (
        player(row[0]),
        *int_or_none(row[1:]),
    )
