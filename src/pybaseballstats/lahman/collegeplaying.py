from pybaseballstats.importer import int_or_none

FILE_NAME = "lahman_1871-2023_csv/CollegePlaying.csv"

TABLE_NAME = "l_college_playing"

TABLE = """CREATE TABLE IF NOT EXISTS l_college_playing
            (
                player_id TEXT    NOT NULL,
                school_id TEXT    NOT NULL,
                year      INTEGER NOT NULL,
                PRIMARY KEY (player_id, school_id, year)
            );"""


def row_function(row, school, player, **kwargs):
    return (
        player(row[0]),
        school(row[1]),
        int_or_none(row[2]),
    )
