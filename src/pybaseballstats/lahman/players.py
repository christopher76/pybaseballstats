from functools import cache

from pybaseballstats.importer import int_or_none, text_or_none, Importer

FILE_NAME = "lahman_1871-2023_csv/People.csv"

TABLE_NAME = "l_players"

TABLE = """CREATE TABLE IF NOT EXISTS l_players
            (
                id            INTEGER NOT NULL PRIMARY KEY,
                player_key    TEXT    NOT NULL,
                birth_year    INTEGER,
                birth_month   INTEGER,
                birth_day     INTEGER,
                birth_city    TEXT,
                birth_country TEXT,
                birth_state   TEXT,
                death_year    INTEGER,
                death_month   INTEGER,
                death_day     INTEGER,
                death_country TEXT,
                death_state   TEXT,
                death_city    TEXT,
                name_first    TEXT,
                name_last     TEXT    NOT NULL,
                name_given    TEXT,
                weight        INTEGER,
                height        INTEGER,
                bats          TEXT,
                throws        TEXT,
                debut         TEXT,
                bbref_id      TEXT,
                final_game    TEXT,
                retro_id      TEXT, -- TODO FK to rs_player
                UNIQUE (player_key)
            );"""

INDEXES = ["""CREATE INDEX idx_l_player_1 ON l_players (player_key);"""]


def row_function(row, **kwargs):
    return (
        int_or_none(row[0]),
        text_or_none(row[1]),
        *int_or_none(row[2:5]),
        *text_or_none(row[5:8]),
        *int_or_none(row[8:11]),
        *text_or_none(row[11:17]),
        *int_or_none(row[17:19]),
        *text_or_none(row[19:]),
    )


FIXES = [
    (
        "INSERT INTO l_players (player_key, name_last) VALUES (?, ?);",
        ("thompan01", "thompan01"),
    ),
    (
        "INSERT INTO l_players (player_key, name_last) VALUES (?, ?);",
        ("fowlebu99", "fowlebu99"),
    ),
]


@cache
def lookup(context: Importer, player_key):
    row = context.execute_single(
        "SELECT id FROM l_players WHERE player_key = ?;", (player_key,)
    )
    if row:
        return row[0]
    else:
        raise KeyError
