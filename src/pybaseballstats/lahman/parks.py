from functools import cache

from pybaseballstats.importer import int_or_none, text_or_none, Importer

FILE_NAME = "lahman_1871-2023_csv/Parks.csv"

TABLE_NAME = "l_parks"

TABLE = """CREATE TABLE IF NOT EXISTS l_parks
            (
                id       INTEGER NOT NULL PRIMARY KEY,
                park_key TEXT    NOT NULL,
                name     TEXT    NOT NULL,
                city     TEXT    NOT NULL,
                state    TEXT,
                country  TEXT    NOT NULL,
                alias    TEXT,
                UNIQUE (park_key)
            );"""

INDEXES = ["""CREATE INDEX idx_l_parks_1 ON l_parks (park_key);"""]


def row_function(row, **kwargs):
    return (
        int_or_none(row[0]),
        *text_or_none(row[2:7]),
        text_or_none(row[1]),
    )


@cache
def lookup(context: Importer, park_key):
    row = context.execute_single(
        "SELECT id FROM l_parks WHERE park_key = ?;", (park_key,)
    )
    if row:
        return row[0]
    else:
        raise KeyError()
