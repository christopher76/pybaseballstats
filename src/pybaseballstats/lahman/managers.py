from pybaseballstats.importer import int_or_none, text_or_none

FILE_NAME = "lahman_1871-2023_csv/Managers.csv"

TABLE_NAME = "l_managers"

TABLE = """CREATE TABLE IF NOT EXISTS l_managers
            (
                player_id      TEXT    NOT NULL,
                year           INTEGER NOT NULL,
                team_id        INTEGER NOT NULL,
                in_season      INTEGER NOT NULL,
                G              INTEGER NOT NULL,
                W              INTEGER NOT NULL,
                L              INTEGER NOT NULL,
                rank           INTEGER NOT NULL,
                player_manager TEXT    NOT NULL,
                PRIMARY KEY (player_id, year, team_id, in_season)
            );"""


def row_function(row, player, team, **kwargs):
    return (
        player(row[0]),
        int_or_none(row[1]),
        team(int(row[1]), row[2], row[3]),
        *int_or_none(row[4:9]),
        text_or_none(row[9]),
    )
