from functools import cache

from pybaseballstats.importer import text_or_none, Importer

FILE_NAME = "lahman_1871-2023_csv/TeamsFranchises.csv"

TABLE_NAME = "l_franchises"

TABLE = """CREATE TABLE IF NOT EXISTS l_franchises
            (
                id            INTEGER NOT NULL PRIMARY KEY,
                franchise_key TEXT    NOT NULL,
                name          TEXT    NOT NULL,
                active        TEXT,
                NAassoc       TEXT
            );"""

INDEXES = ["""CREATE INDEX idx_l_franchises_1 ON l_franchises (franchise_key);"""]


def row_function(row, **kwargs):
    return (
        None,
        *text_or_none(row[0:]),
    )


@cache
def lookup(context: Importer, franchise_key):
    row = context.execute_single(
        "SELECT id FROM l_franchises WHERE franchise_key = ?;", (franchise_key,)
    )
    if row:
        return row[0]
    else:
        raise KeyError
