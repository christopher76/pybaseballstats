from pybaseballstats.importer import int_or_none, text_or_none

FILE_NAME = "lahman_1871-2023_csv/TeamsHalf.csv"

TABLE_NAME = "l_teams_half"

TABLE = """CREATE TABLE IF NOT EXISTS l_teams_half
                (
                    id INTEGER NOT NULL PRIMARY KEY,
                    year INTEGER NOT NULL,
                    team_id INTEGER NOT NULL,
                    half INTEGER NOT NULL,
                    division_key TEXT NOT NULL,
                    DivWin TEXT NOT NULL,
                    Rank INTEGER NOT NULL,
                    G INTEGER NOT NULL,
                    W INTEGER NOT NULL,
                    L INTEGER NOT NULL,
                    UNIQUE (year, team_id, half)
                );"""


def row_function(row, team, **kwargs):
    return (
        None,
        int_or_none(row[0]),
        team(int(row[0]), row[2], row[1]),
        int_or_none(row[3]),
        *text_or_none(row[4:6]),
        *int_or_none(row[6:]),
    )
