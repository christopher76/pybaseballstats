from pybaseballstats.importer import int_or_none, text_or_none

FILE_NAME = "lahman_1871-2023_csv/SeriesPost.csv"

TABLE_NAME = "l_series_post"

TABLE = """CREATE TABLE IF NOT EXISTS l_series_post
            (
                year           INTEGER NOT NULL,
                round          TEXT    NOT NULL,
                winner_team_id TEXT    NOT NULL,
                loser_team_id  TEXT    NOT NULL,
                wins           INTEGER NOT NULL,
                losses         INTEGER NOT NULL,
                ties           INTEGER NOT NULL,
                PRIMARY KEY (year, round)
            );"""


def row_function(row, team, **kwargs):
    return (
        int_or_none(row[0]),
        text_or_none(row[1]),
        team(int(row[0]), row[2], row[3]),
        team(int(row[0]), row[4], row[5]),
        *int_or_none(row[6:]),
    )
