from pybaseballstats.importer import int_or_none

FILE_NAME = "lahman_1871-2023_csv/Appearances.csv"

TABLE_NAME = "l_appearances"

TABLE = """CREATE TABLE IF NOT EXISTS l_appearances
            (
                year      INTEGER NOT NULL,
                team_id   INTEGER NOT NULL,
                player_id INTEGER NOT NULL,
                G_all     INTEGER NOT NULL,
                GS        INTEGER,
                G_batting INTEGER NOT NULL,
                G_defense INTEGER,
                G_p       INTEGER NOT NULL,
                G_c       INTEGER NOT NULL,
                G_1b      INTEGER NOT NULL,
                G_2b      INTEGER NOT NULL,
                G_3b      INTEGER NOT NULL,
                G_ss      INTEGER NOT NULL,
                G_lf      INTEGER NOT NULL,
                G_cf      INTEGER NOT NULL,
                G_rf      INTEGER NOT NULL,
                G_of      INTEGER NOT NULL,
                G_dh      INTEGER,
                G_ph      INTEGER,
                G_pr      INTEGER,
                PRIMARY KEY (year, team_id, player_id)
            );"""


def row_function(row, player, team, **kwargs):
    return (
        int_or_none(row[0]),
        team(int(row[0]), row[1], row[2]),
        player(row[3]),
        *int_or_none(row[4:]),
    )
