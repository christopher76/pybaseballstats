from pybaseballstats.importer import int_or_none, text_or_none

FILE_NAME = "lahman_1871-2023_csv/HomeGames.csv"

TABLE_NAME = "l_home_games"

TABLE = """CREATE TABLE IF NOT EXISTS l_home_games
            (
                year       INTEGER NOT NULL,
                team_id    INTEGER NOT NULL,
                park_id    TEXT    NOT NULL,
                span_first TEXT    NOT NULL,
                span_last  TEXT    NOT NULL,
                games      INTEGER NOT NULL,
                openings   INTEGER NOT NULL,
                attendance INTEGER NOT NULL,
                PRIMARY KEY (year, team_id, park_id)
            );"""


def row_function(row, park, team, **kwargs):
    return (
        int_or_none(row[0]),
        team(int(row[0]), row[2], row[1]),
        park(row[3]),
        *text_or_none(row[4:6]),
        *int_or_none(row[6:]),
    )
