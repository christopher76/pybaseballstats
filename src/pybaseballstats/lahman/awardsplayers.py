from pybaseballstats.importer import int_or_none, text_or_none

FILE_NAME = "lahman_1871-2023_csv/AwardsPlayers.csv"

TABLE_NAME = "l_awards_players"

TABLE = """CREATE TABLE IF NOT EXISTS l_awards_players
            (
                player_id TEXT    NOT NULL,
                award     TEXT    NOT NULL,
                year      INTEGER NOT NULL,
                league    TEXT,
                tie       TEXT,
                notes     TEXT
            );"""  # NO PRIMARY KEY


def row_function(row, player, **kwargs):
    return (
        player(row[0]),
        text_or_none(row[1]),
        int_or_none(row[2]),
        *text_or_none(row[3:]),
    )
