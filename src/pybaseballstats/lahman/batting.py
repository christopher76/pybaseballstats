from pybaseballstats.importer import int_or_none

FILE_NAME = "lahman_1871-2023_csv/Batting.csv"

TABLE_NAME = "l_batting"

TABLE = """CREATE TABLE IF NOT EXISTS l_batting
            (
                player_id INTEGER NOT NULL,
                year      INTEGER NOT NULL,
                stint     INTEGER NOT NULL,
                team_id   INTEGER NOT NULL,
                G         INTEGER NOT NULL,
                G_batting INTEGER,
                AB        INTEGER NOT NULL,
                R         INTEGER NOT NULL,
                H         INTEGER NOT NULL,
                Dbl       INTEGER NOT NULL,
                Trpl      INTEGER NOT NULL,
                HR        INTEGER NOT NULL,
                RBI       INTEGER,
                SB        INTEGER,
                CS        INTEGER,
                BB        INTEGER NOT NULL,
                SO        INTEGER,
                IBB       INTEGER,
                HBP       INTEGER,
                SH        INTEGER,
                SF        INTEGER,
                GIDP      INTEGER,
                PRIMARY KEY (player_id, year, stint)
            );"""


def row_function(row, player, team, **kwargs):
    return (
        player(row[0]),
        *int_or_none(row[1:3]),
        team(int(row[1]), row[3], row[4]),
        *int_or_none(row[5:-1]),
    )
