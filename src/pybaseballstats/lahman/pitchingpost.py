from pybaseballstats.importer import int_or_none, text_or_none, float_or_none

FILE_NAME = "lahman_1871-2023_csv/PitchingPost.csv"

TABLE_NAME = "l_pitching_post"

TABLE = """CREATE TABLE IF NOT EXISTS l_pitching_post
            (
                player_id TEXT    NOT NULL,
                year      INTEGER NOT NULL,
                round     TEXT    NOT NULL,
                team_id   INTEGER NOT NULL,
                W         INTEGER NOT NULL,
                L         INTEGER NOT NULL,
                G         INTEGER NOT NULL,
                GS        INTEGER NOT NULL,
                CG        INTEGER NOT NULL,
                SHO       INTEGER NOT NULL,
                SV        INTEGER NOT NULL,
                IPouts    INTEGER NOT NULL,
                H         INTEGER NOT NULL,
                ER        INTEGER NOT NULL,
                HR        INTEGER NOT NULL,
                BB        INTEGER NOT NULL,
                SO        INTEGER NOT NULL,
                BAOpp     NUMERIC,
                ERA       NUMERIC,
                IBB       INTEGER,
                WP        INTEGER,
                HBP       INTEGER,
                BK        INTEGER,
                BFP       INTEGER,
                GF        INTEGER NOT NULL,
                R         INTEGER NOT NULL,
                SH        INTEGER,
                SF        INTEGER,
                GIDP      INTEGER,
                PRIMARY KEY (player_id, year, round)
            );"""


def row_function(row, player, team, **kwargs):
    return (
        player(row[0]),  # playerID
        int_or_none(row[1]),  # yeardID
        text_or_none(row[2]),  # round
        team(int(row[1]), row[3], row[4]),  # teamid
        *int_or_none(row[5:18]),  # win
        *float_or_none(row[18:20]),  # BAOpp
        *int_or_none(row[20:]),  # IBB
    )
