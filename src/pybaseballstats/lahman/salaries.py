from pybaseballstats.importer import int_or_none

FILE_NAME = "lahman_1871-2023_csv/Salaries.csv"

TABLE_NAME = "l_salaries"

TABLE = """CREATE TABLE IF NOT EXISTS l_salaries
            (
                year      INTEGER NOT NULL,
                team_id   INTEGER NOT NULL,
                player_id TEXT    NOT NULL,
                salary    INTEGER NOT NULL,
                PRIMARY KEY (player_id, year, team_id)
            );"""


def row_function(row, player, team, **kwargs):
    return (
        int_or_none(row[0]),
        team(int(row[0]), row[1], row[2]),
        player(row[3]),
        int_or_none(row[4]),
    )
