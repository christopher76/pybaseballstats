from functools import partial

# Sean Lahman
# This database is copyright 1996-2024 by Sean Lahman.
# This work is licensed under a Creative Commons Attribution-ShareAlike 3.0
# Unported License. For details see: http://creativecommons.org/licenses/by-sa/3.0/
# For licensing information or further information, contact Sean Lahman at: seanlahman@gmail.com

from pybaseballstats.lahman import (
    teams,
    allstarfull,
    appearances,
    awardssharemanagers,
    batting,
    fielding,
    fieldingofsplit,
    managershalf,
)
from pybaseballstats.lahman import (
    pitching,
    managers,
    fieldingof,
    awardsmanagers,
    collegeplaying,
    awardsshareplayers,
    franchises,
    teamshalf,
    players,
    seriespost,
    battingpost,
    schools,
    salaries,
    pitchingpost,
    awardsplayers,
    halloffame,
    parks,
    homegames,
    fieldingpost,
)
from pybaseballstats.importer import Importer, Archive

DATA_URL = (
    "https://www.dropbox.com/scl/fi/hy0sxw6gaai7ghemrshi8/lahman_1871-2023_csv.7z"
    "?rlkey=edw1u63zzxg48gvpcmr3qpnhz&dl=1"
)


def load(context: Importer):

    archive = Archive(context.confirm_download(DATA_URL), "7z", encoding="cp1252")

    mods = [
        parks,
        schools,
        players,
        franchises,
        teams,
        allstarfull,
        appearances,
        awardsmanagers,
        awardsplayers,
        awardssharemanagers,
        awardsshareplayers,
        batting,
        battingpost,
        collegeplaying,
        fielding,
        fieldingof,
        fieldingofsplit,
        fieldingpost,
        halloffame,
        homegames,
        managers,
        managershalf,
        pitching,
        pitchingpost,
        salaries,
        seriespost,
        teamshalf,
    ]

    lookup_park = partial(parks.lookup, context)
    lookup_school = partial(schools.lookup, context)
    lookup_player = partial(players.lookup, context)
    lookup_franchise = partial(franchises.lookup, context)
    lookup_team = partial(teams.lookup, context)

    print("Processing data files... ")
    for mod in mods:
        context.execute_nonquery(mod.TABLE)
        if hasattr(mod, "INDEXES"):
            for index in mod.INDEXES:
                context.execute_nonquery(index)
        context.load_table_from_archive(
            archive,
            mod.FILE_NAME,
            mod.TABLE_NAME,
            partial(
                mod.row_function,
                park=lookup_park,
                school=lookup_school,
                player=lookup_player,
                franchise=lookup_franchise,
                team=lookup_team,
            ),
        )
        if hasattr(mod, "FIXES"):
            for fix in mod.FIXES:
                context.execute_nonquery(fix[0], fix[1])
