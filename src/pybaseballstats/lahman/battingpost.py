from pybaseballstats.importer import int_or_none, text_or_none

FILE_NAME = "lahman_1871-2023_csv/BattingPost.csv"

TABLE_NAME = "l_batting_post"

TABLE = """CREATE TABLE IF NOT EXISTS l_batting_post
            (
                year      INTEGER NOT NULL,
                round     TEXT    NOT NULL,
                player_id TEXT    NOT NULL,
                team_id   INTEGER NOT NULL,
                G         INTEGER NOT NULL,
                AB        INTEGER NOT NULL,
                R         INTEGER NOT NULL,
                H         INTEGER NOT NULL,
                Dbl       INTEGER NOT NULL,
                Trpl      INTEGER NOT NULL,
                HR        INTEGER NOT NULL,
                RBI       INTEGER NOT NULL,
                SB        INTEGER NOT NULL,
                CS        INTEGER,
                BB        INTEGER NOT NULL,
                SO        INTEGER NOT NULL,
                IBB       INTEGER NOT NULL,
                HBP       INTEGER,
                SH        INTEGER,
                SF        INTEGER,
                GID       INTEGER,
                PRIMARY KEY (year, round, player_id)
            );"""


def row_function(row, player, team, **kwargs):
    return (
        int_or_none(row[0]),
        text_or_none(row[1]),
        player(row[2]),
        team(int(row[0]), row[3], row[4]),
        *int_or_none(row[5:]),
    )
