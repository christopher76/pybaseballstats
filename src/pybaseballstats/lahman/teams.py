from functools import cache

from pybaseballstats.importer import (
    int_or_none,
    text_or_none,
    Importer,
    float_or_none,
)

FILE_NAME = "lahman_1871-2023_csv/Teams.csv"

TABLE_NAME = "l_teams"

TABLE = """CREATE TABLE IF NOT EXISTS l_teams
                (
                    id INTEGER NOT NULL PRIMARY KEY,
                    year INTEGER,
                    league TEXT NOT NULL,
                    team_key TEXT NOT NULL,
                    franchise_id INTEGER NOT NULL,
                    division_key TEXT,
                    Rank INTEGER NOT NULL,
                    G INTEGER NOT NULL,
                    Ghome INTEGER,
                    W INTEGER NOT NULL,
                    L INTEGER NOT NULL,
                    DivWin TEXT,
                    WCWin TEXT,
                    LgWin TEXT,
                    WSWin TEXT,
                    R INTEGER NOT NULL,
                    AB INTEGER NOT NULL,
                    H INTEGER NOT NULL,
                    Dbl INTEGER NOT NULL,
                    Trpl INTEGER NOT NULL,
                    HR INTEGER NOT NULL,
                    BB INTEGER NOT NULL,
                    SO INTEGER,
                    SB INTEGER,
                    CS INTEGER,
                    HBP INTEGER,
                    SF INTEGER,
                    RA INTEGER NOT NULL,
                    ER INTEGER NOT NULL,
                    ERA NUMERIC NOT NULL,
                    CG INTEGER NOT NULL,
                    SHO INTEGER NOT NULL,
                    SV INTEGER NOT NULL,
                    IPouts INTEGER NOT NULL,
                    HA INTEGER NOT NULL,
                    HRA INTEGER NOT NULL,
                    BBA INTEGER NOT NULL,
                    SOA INTEGER NOT NULL,
                    E INTEGER NOT NULL,
                    DP INTEGER NOT NULL,
                    FP NUMERIC NOT NULL,
                    name TEXT NOT NULL,
                    park TEXT,
                    attendance INTEGER,
                    BPF INTEGER NOT NULL,
                    PPF INTEGER NOT NULL,
                    teamIDBR TEXT NOT NULL,
                    teamIDlahman45 TEXT NOT NULL,
                    teamIDretro TEXT NOT NULL,
                    UNIQUE (year, team_key, league)
                );"""

INDEXES = [
    """CREATE INDEX idx_l_teams_1
                    ON l_teams (year, team_key, league);
                """
]


def row_function(row, franchise, **kwargs):
    return (
        None,
        int_or_none(row[0]),  # YearId
        *text_or_none(row[1:3]),  # lgId
        franchise(row[3]),  # franchise
        text_or_none(row[4]),  # DivId
        *int_or_none(row[5:10]),  # Rank
        *text_or_none(row[10:14]),  # DivWin
        *int_or_none(row[14:28]),  # R
        float_or_none(row[28]),  # ERA
        *int_or_none(row[29:39]),  # CG
        float_or_none(row[39]),  # FP
        *text_or_none(row[40:42]),  # Name
        *int_or_none(row[42:45]),  # Attendance
        *text_or_none(row[45:]),  # Name
    )


@cache
def lookup(context: Importer, year, team_key, league):

    # Individual Fixes

    # Fix for SeriesPost HOU NL 2022 -> AL
    if year >= 2013 and (team_key, league) == ("HOU", "NL"):
        league = "AL"

    # Fix for AllStarsFull TEAMID = MLN
    if (team_key, league) == ("MLN", "NL"):
        team_key = "ML1"

    # Fix for AllStarsFull dicksja01,1997
    # erstada01,1998
    # percitr01,1998
    if 1997 <= year <= 2004 and (team_key, league) == ("LAA", "AL"):
        team_key = "ANA"

    # Fix for AllStarsFull freeseda01 2021
    if (team_key, league) == ("SLN", "AL"):
        league = "NL"

    row = context.execute_single(
        "SELECT id FROM l_teams WHERE year = ? AND team_key = ? AND league = ?;",
        (year, team_key, league),
    )
    if row:
        return row[0]
    else:
        # Bulk Fixes: Search on franchise_key if team_key not found
        row = context.execute_single(
            """SELECT t.id, t.team_key, t.league FROM l_teams t INNER JOIN l_franchises f 
                ON t.franchise_id = f.id 
                WHERE t.year = ? AND f.franchise_key = ? AND t.league = ?;""",
            (year, team_key, league),
        )
        if row:
            return row[0]
        else:
            raise KeyError
