from pybaseballstats.importer import int_or_none, text_or_none

FILE_NAME = "lahman_1871-2023_csv/AwardsSharePlayers.csv"

TABLE_NAME = "l_awards_share_players"

TABLE = """CREATE TABLE IF NOT EXISTS l_awards_share_players
            (
                award       TEXT    NOT NULL,
                year        TEXT    NOT NULL,
                league      TEXT    NOT NULL,
                player_id   INTEGER NOT NULL,
                points_won  INTEGER NOT NULL,
                points_max  INTEGER NOT NULL,
                votes_first INTEGER,
                PRIMARY KEY (award, year, league, player_id)
            );"""


def row_function(row, player, **kwargs):
    return (
        *text_or_none(row[0:3]),
        player(row[3]),
        *int_or_none(row[4:]),
    )
