from pybaseballstats.importer import int_or_none, text_or_none

FILE_NAME = "lahman_1871-2023_csv/AllstarFull.csv"

TABLE_NAME = "l_all_star_full"

TABLE = """CREATE TABLE IF NOT EXISTS l_all_star_full
            (
                player_id    INTEGER NOT NULL,
                year         INTEGER NOT NULL,
                game_num     INTEGER NOT NULL,
                game_id      TEXT,
                team_id      INTEGER NOT NULL,
                GP           INTEGER NOT NULL,
                starting_pos INTEGER
            );"""


def row_function(row, player, team, **kwargs):
    return (
        player(row[0]),
        *int_or_none(row[1:3]),
        text_or_none(row[3]),
        team(int(row[1]), row[4], row[5]),
        *int_or_none(row[6:]),
    )
