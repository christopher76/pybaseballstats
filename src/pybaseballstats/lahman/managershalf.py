from pybaseballstats.importer import int_or_none, text_or_none

FILE_NAME = "lahman_1871-2023_csv/ManagersHalf.csv"

TABLE_NAME = "l_managers_half"

TABLE = """CREATE TABLE IF NOT EXISTS l_managers_half
            (
                player_id TEXT    NOT NULL,
                year      INTEGER NOT NULL,
                team_id   INTEGER NOT NULL,
                in_season INTEGER NOT NULL,
                half      INTEGER NOT NULL,
                G         INTEGER NOT NULL,
                W         INTEGER NOT NULL,
                L         INTEGER NOT NULL,
                rank      INTEGER NOT NULL,
                PRIMARY KEY (player_id, year, half, team_id)
            );"""


def row_function(row, team, **kwargs):
    return (
        text_or_none(row[0]),
        int_or_none(row[1]),
        team(int(row[1]), row[2], row[3]),
        *int_or_none(row[4:9]),
        text_or_none(row[9]),
    )
