from pybaseballstats.importer import int_or_none, text_or_none

FILE_NAME = "lahman_1871-2023_csv/FieldingPost.csv"

TABLE_NAME = "l_fielding_post"

TABLE = """CREATE TABLE IF NOT EXISTS l_fielding_post
            (
                player_id TEXT    NOT NULL,
                year      INTEGER NOT NULL,
                team_id   INTEGER NOT NULL,
                round     TEXT    NOT NULL,
                POS       TEXT    NOT NULL,
                G         INTEGER NOT NULL,
                GS        INTEGER NOT NULL,
                InnOuts   INTEGER,
                PO        INTEGER NOT NULL,
                A         INTEGER NOT NULL,
                E         INTEGER NOT NULL,
                DP        INTEGER NOT NULL,
                PB        INTEGER,
                WP        INTEGER,
                SB        INTEGER,
                CS        INTEGER,
                PRIMARY KEY (player_id, year, round, POS)
            );"""


def row_function(row, player, team, **kwargs):
    return (
        player(row[0]),
        int_or_none(row[1]),
        team(int(row[1]), row[2], row[3]),
        *text_or_none(row[4:6]),
        *int_or_none(row[6:]),
    )
