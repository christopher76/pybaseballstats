from functools import cache

from pybaseballstats.importer import text_or_none, Importer

FILE_NAME = "lahman_1871-2023_csv/Schools.csv"

TABLE_NAME = "l_schools"

TABLE = """CREATE TABLE IF NOT EXISTS l_schools
            (
                id         INTEGER NOT NULL PRIMARY KEY,
                school_key TEXT    NOT NULL,
                name_full  TEXT    NOT NULL,
                city       TEXT    NOT NULL,
                state      TEXT    NOT NULL,
                country    TEXT    NOT NULL,
                UNIQUE (school_key)
            );"""

INDEXES = ["""CREATE INDEX idx_l_schools_1 ON l_schools (school_key);"""]


def row_function(row, **kwargs):
    if len(row) > 5:
        return (
            None,
            text_or_none(row[0]),
            text_or_none(row[1]) + "," + text_or_none(row[2]),
            *text_or_none(row[3:]),
        )
    else:
        return (
            None,
            *text_or_none(row[0:5]),
        )


_insert_sql = "INSERT INTO l_schools (school_key, name_full, city, state, country) VALUES (?, ?, ?, ?, ?);"
FIXES = [
    (
        _insert_sql,
        ("ctpostu", "ctpostu", "N/A", "N/A", "N/A"),
    ),
    (
        _insert_sql,
        ("txutper", "txutper", "N/A", "N/A", "N/A"),
    ),
    (
        _insert_sql,
        ("txrange", "txrange", "N/A", "N/A", "N/A"),
    ),
    (
        _insert_sql,
        ("caallia", "caallia", "N/A", "N/A", "N/A"),
    ),
]


@cache
def lookup(context: Importer, school_key):
    row = context.execute_single(
        "SELECT id FROM l_schools WHERE school_key = ?;", (school_key,)
    )
    if row:
        return row[0]
    else:
        raise KeyError
