from pybaseballstats.importer import int_or_none, text_or_none

FILE_NAME = "lahman_1871-2023_csv/FieldingOFsplit.csv"

TABLE_NAME = "l_fielding_of_split"

TABLE = """CREATE TABLE IF NOT EXISTS l_fielding_of_split
            (
                player_id TEXT    NOT NULL,
                year      INTEGER NOT NULL,
                stint     INTEGER NOT NULL,
                team_id   INTEGER NOT NULL,
                POS       TEXT    NOT NULL,
                G         INTEGER NOT NULL,
                GS        INTEGER NOT NULL,
                InnOuts   INTEGER NOT NULL,
                PO        INTEGER NOT NULL,
                A         INTEGER NOT NULL,
                E         INTEGER NOT NULL,
                DP        INTEGER NOT NULL,
                PB        INTEGER,
                WP        INTEGER,
                SB        INTEGER,
                CS        INTEGER,
                ZR        INTEGER,
                PRIMARY KEY (player_id, year, stint, POS)
            );"""


def row_function(row, player, team, **kwargs):
    return (
        player(row[0]),
        *int_or_none(row[1:3]),
        team(int(row[1]), row[3], row[4]),
        text_or_none(row[5]),
        *int_or_none(row[6:]),
    )
