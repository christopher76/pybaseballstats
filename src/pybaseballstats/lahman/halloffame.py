from pybaseballstats.importer import int_or_none, text_or_none

FILE_NAME = "lahman_1871-2023_csv/HallOfFame.csv"

TABLE_NAME = "l_hall_of_fame"

TABLE = """CREATE TABLE IF NOT EXISTS l_hall_of_fame
            (
                player_id   TEXT    NOT NULL,
                year        INTEGER NOT NULL,
                voted_by    TEXT    NOT NULL,
                ballots     INTEGER,
                needed      INTEGER,
                votes       INTEGER,
                inducted    TEXT,
                category    TEXT    NOT NULL,
                needed_note TEXT,
                PRIMARY KEY (player_id, year, voted_by)
            );"""


def row_function(row, player, **kwargs):
    return (
        player(row[0]),
        int_or_none(row[1]),
        text_or_none(row[2]),
        *int_or_none(row[3:6]),
        *text_or_none(row[6:]),
    )
