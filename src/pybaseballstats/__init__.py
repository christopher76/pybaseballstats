from pathlib import Path

from pybaseballstats.importer import Importer
from pybaseballstats import retrosheet, lahman


def download_and_import(
    *,
    output_sqlite_file="baseball.sqlite",
    download_folder="downloads",
    clear_downloads=False,
):
    with Importer.initialize(
        Path(download_folder), Path(output_sqlite_file), clear_downloads
    ) as context:
        retrosheet.load(context)
        lahman.load(context)
