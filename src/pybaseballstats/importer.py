import csv
from contextlib import contextmanager
import shutil
import sqlite3
from io import StringIO
from pathlib import Path
from urllib.parse import urlparse
from zipfile import ZipFile

import requests
from py7zr import py7zr


class Archive:
    def __init__(self, source_file_path: Path, mode="zip", *, encoding="utf8"):
        self._path = source_file_path
        self._mode = mode
        self._encoding = encoding

    def get_names(self):
        if self._mode == "7z":
            with py7zr.SevenZipFile(self._path, mode="r") as z:
                return z.getnames()
        elif self._mode == "zip":
            with ZipFile(self._path) as z:
                return z.namelist()
        else:
            raise ValueError("Unsupported mode")

    def extract_file(self, file_name):
        if self._mode == "7z":
            with py7zr.SevenZipFile(self._path, mode="r") as z:
                o = z.read([file_name])
                return o[file_name].read().decode(self._encoding)

        elif self._mode == "zip":
            with ZipFile(self._path) as z:
                return z.read(file_name).decode(self._encoding)
        else:
            raise ValueError("Unsupported mode")


class Importer:

    @classmethod
    @contextmanager
    def initialize(cls, source_dir_path, db_file_path, clear_downloads_dir=False):
        i = cls(source_dir_path, db_file_path, clear_downloads_dir)
        yield i
        i._close()
        print("Complete!")

    def __init__(
        self, source_dir_path: Path, db_file_path: Path, clear_source_dir=False
    ):
        self.path = source_dir_path
        if clear_source_dir:
            shutil.rmtree(self.path, ignore_errors=True)
        self.path.mkdir(parents=True, exist_ok=True)

        if db_file_path.exists():
            db_file_path.unlink()
        self._db = sqlite3.connect(db_file_path)

    def _close(self):
        self._db.close()

    def confirm_download(
        self, url, *, dest_file_path="", overwrite=False, change_extension=""
    ):

        if not dest_file_path:
            o = urlparse(url)
            dest_file_path = Path(o.scheme + "://" + o.netloc + o.path).name.lower()

        dest_file_path = (
            Path(dest_file_path).stem + "." + change_extension
            if change_extension
            else dest_file_path
        )
        dest_file_path = self.path.joinpath(dest_file_path)
        if dest_file_path.exists() and not overwrite:
            print("File Exists: ", dest_file_path)
        else:
            print(f"Downloading {url} to {dest_file_path}")
            response = requests.get(url, stream=True)
            with open(dest_file_path, mode="wb") as file:
                for chunk in response.iter_content(chunk_size=10 * 1024):
                    file.write(chunk)
        return dest_file_path

    def execute_nonquery(self, sql, params=None):
        cursor = self._db.cursor()
        cursor.execute(sql, params if params else ())
        self._db.commit()
        cursor.close()

    def execute_single(self, sql, params):
        cursor = self._db.cursor()
        cursor.execute(sql, params)
        row = cursor.fetchone()
        cursor.close()
        return row

    def load_table_from_file(
        self,
        source_file_path: Path,
        table_name,
        row_function,
        *,
        skip_first_row=True,
        encoding="utf8",
    ):
        with open(source_file_path, "r", encoding=encoding) as file:
            reader = csv.reader(file)
            self.load_table_from_reader(
                reader, table_name, row_function, skip_first_row=skip_first_row
            )

    def load_table_from_archive(
        self,
        archive: Archive,
        file_name,
        table_name,
        row_function,
        *,
        skip_first_row=True,
    ):
        reader = csv.reader(StringIO(archive.extract_file(file_name)))
        self.load_table_from_reader(
            reader, table_name, row_function, skip_first_row=skip_first_row
        )

    def load_table_from_reader(
        self,
        reader,
        table_name,
        row_function,
        *,
        skip_first_row=True,
    ):
        if skip_first_row:
            next(reader)
        rows = [row_function(row) for row in reader]

        sql = f"INSERT INTO {table_name} VALUES({", ".join(["?"] * len(rows[0]))})"

        cursor = self._db.cursor()
        cursor.executemany(sql, rows)
        self._db.commit()
        cursor.close()


def int_or_none(val):
    if isinstance(val, list):
        return [int_or_none(v) for v in val]
    return None if val == "" or val == "-1" else int(val)


def text_or_none(val):
    if isinstance(val, list):
        return [text_or_none(v) for v in val]
    return None if not val else val


def float_or_none(val):
    if isinstance(val, list):
        return [float_or_none(v) for v in val]
    return None if val == "" else val
